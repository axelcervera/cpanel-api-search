# cPanel API Search

This is a simple bash function with the goal to make it simpler and quicker to locate the correct syntax for cPanel provided APIs. 

The APIs are sourced from the following official cPanel Developer Documentation articles:

WHM API 1
cPanel API 2
UAPI

This is a very early version, most of the code can still be reworked and made more efficient.

I intend to re-write this in Python, sometime in the future.